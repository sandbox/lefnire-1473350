
; Drupal version
core = 7.x
api = 2

; Contrib modules
projects[debut][subdir] = contrib
projects[debut_personnel][subdir] = contrib
projects[debut_personnel][version] = 1.0-beta1
projects[debut_media][subdir] = contrib
projects[ctools][subdir] = contrib
projects[features][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[views][subdir] = contrib
