<?php
/**
 * @file
 * debut_personnel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function debut_personnel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function debut_personnel_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function debut_personnel_node_info() {
  $items = array(
    'personnel' => array(
      'name' => t('Personnel'),
      'base' => 'node_content',
      'description' => t('Personnel such as staff, advisory board, members, constituents, etc'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
